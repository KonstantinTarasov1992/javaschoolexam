package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers == null || inputNumbers.contains(null) || inputNumbers.isEmpty())
            throw new CannotBuildPyramidException();

        try {
            Collections.sort(inputNumbers);
            /*
            Create an array that contains the correct sizes of the resulting array.
            The size must be commensurable with N*((N+1)/2)
             */
            ArrayList<Integer> alist = new ArrayList<>();
            for (Integer i = 1; i <= inputNumbers.size(); i++) {
                alist.add(i * (i+1)/2);
            }
            /*
            Initialize the size of the array resulting array.
             */
            int rows = alist.indexOf(inputNumbers.size()) + 1;
            int columns = 2 * alist.indexOf(inputNumbers.size()) + 1;
            int[][] res = new int[rows][columns];
            int index = 0;
            /*
            Fill the array starting at position N-i-1
             */
            for (int i = 0; i < rows; i++) {
                for (int j = rows-i-1, k = 1;  k <= i+1 ; j+=2, k++) {
                    res[i][j] = inputNumbers.get(index);
                    index++;
                }
            }
            return res;
        }
        catch (Throwable t){
            throw new CannotBuildPyramidException();
        }
    }
}





