package com.tsystems.javaschool.tasks.zones;

import java.util.LinkedList;
import java.util.List;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        // TODO : Implement your solution here
        for(int j = 0; j < requestedZoneIds.size(); j++) {
            // wereHere - list of zones, where we were
            List<Integer> visited = new LinkedList<>();
            visited.add(requestedZoneIds.get(j));

            Zone curZone = findZoneById(zoneState, requestedZoneIds.get(j));
            for (int i = 0; i < requestedZoneIds.size(); i++) {
                int tempId = requestedZoneIds.get(i);
                // if we didn't here
                if (!visited.contains(tempId)) {
                    // find new zone
                    Zone tempZone = findZoneById(zoneState, tempId);
                    // if curZone and tempZone are neighbors
                    if (((curZone.getNeighbours() != null && curZone.getNeighbours().contains(tempId)) ||
                            (tempZone.getNeighbours() != null && tempZone.getNeighbours().contains(curZone.getId())))) {
                        // add new id to list of zones that we visited
                        visited.add(tempId);

                        //We have visited all zones
                        if(visited.size() == requestedZoneIds.size())
                            return true;
                        curZone = tempZone;
                        i = -1;
                    }
                }
            }
        }
        return false;
    }

    private Zone findZoneById(List<Zone> zones, int zoneId){
        for (Zone checkZone : zones)
            if(checkZone.getId() == zoneId)
                return checkZone;
        return null;
    }
}


